# Programmazione concorrente in C

## Name
Esercizi di programmazione concorrente in C su Linux

## Description
Questi semplici esercizi si propongono di mostrare il funzionamento delle più note system call utili a comprendere alcuni concetti della programmazione concorrente in C sotto Linux. Sono pensati per usi didattici non avanzati, potrebbero quindi contenere delle regressioni e delle imprecisioni che tuttavia risultano utili in contesti dimostrativi e didattici

## Usage
È necessario compilare il sorgente (per esempio attraverso il comando GCC) ed eseguirlo in ambiente Linux

## Roadmap
L'intenzione è di pubblicare esercizi anche sugli argomenti precedenti alla IPC

## Authors and acknowledgment
Daniele Nicoli

## License
Creative Commons. For educational purposes

## Project status
Aperto
